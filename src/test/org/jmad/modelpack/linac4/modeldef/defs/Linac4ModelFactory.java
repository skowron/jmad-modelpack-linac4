package org.jmad.modelpack.linac4.modeldef.defs;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

public class Linac4ModelFactory extends JMadModelDefinitionDslSupport {

    {
        name("LINAC4");

        

        init(i -> {
            i.call("aperture/l4t.dbx");
            i.call("elements/l4t.ele");
            i.call("sequence/l4t.seq");
            i.call("beam/beam.beamx");
            i.call("strength/l4t.str");
        });

        sequence("l4t").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.betx(5.53);
                    t.alfx(1.74);
                    t.bety(11.0);
                    t.alfy(-3.0);
                    t.calcAtCenter();
                });
            });
        });

        optics("l4t_2018_v2").isDefault().isDefinedAs(o -> {
            o.call("strength/l4t.str").parseAs(STRENGTHS);
        });

    }

}
