/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.jmad.modelpack.linac4.modeldef.defs;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

public class Linac4fintModelFactory extends JMadModelDefinitionDslSupport {

    {
        name("LINAC4fint");

        

        init(i -> {
            i.call("aperture/l4t.dbx");
            i.call("elements/l4tfint.ele");
            i.call("sequence/l4t.seq");
            i.call("beam/beam.beamx");
            i.call("strength/l4tfint0p4.str");
            i.call("strength/l4tfint0p45.str");
            i.call("strength/l4tfint0p7.str");
            i.call("strength/l4tfint1o6.str");
        });

        sequence("l4t").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.betx(5.53);
                    t.alfx(1.74);
                    t.bety(11.0);
                    t.alfy(-3.0);
                    t.calcAtCenter();
                });
            });
        });

        optics("l4t_2018_fint0p4").isDefault().isDefinedAs(o -> {
            o.call("strength/l4tfint0p4.str").parseAs(STRENGTHS);
        });
        
        optics("l4t_2018_fint0p45").isDefinedAs(o -> {
            o.call("strength/l4tfint0p45.str").parseAs(STRENGTHS);
        });
        
        optics("l4t_2018_fint0p7").isDefinedAs(o -> {
            o.call("strength/l4tfint0p7.str").parseAs(STRENGTHS);
        });
        
        optics("l4t_2018_fint1o6").isDefinedAs(o -> {
            o.call("strength/l4tfint1o6.str").parseAs(STRENGTHS);
        });

    }

}
