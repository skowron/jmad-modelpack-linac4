package org.jmad.modelpack.linac4.modeldef;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class Linac4ModelDefCreator {

    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/org/jmad/modelpack/linac4/modeldef/defs");
    }

}
